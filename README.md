# Arlo

To set up this repo after being downloaded a *composer install* will need to be run.
If you need to install composer full details can be found at https://getcomposer.org/doc/00-intro.md

This installs phpdotenv so a .env file can be used to store the MySQL details.

The .env file will resisde in the root directory.

The details of the .env file are as below you will need to amend the database name and password to match your own machine.

```
# Settings for MySQL
DATABASE_NAME="db_name"
DATABASE_PASSWORD="db_password"
DATABASE_SERVER="localhost"
DATABASE_USERNAME="root"

```

On my development machine I had to set up .htaccess file to direct the results slug to index.php

Here is a sample of the .htaccess file that was on my machine

```
ewriteEngine On
RewriteRule ^results/? http://localhost/index.php [R=301,L]

```

