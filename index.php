<?php

require_once('php/event.php');

$event = new event();
require_once('entry-point/unchanged.php');
$keyword = ($_GET['keyword']) ? $_GET['keyword'] : null;
$location = ($_GET['location']) ? $_GET['location'] : null;
$category = ($_GET['category']) ? $_GET['category'] : null;
$results = $event->search($keyword, $location, $category);
$locations = $event->getLocations();
$categories = $event->getCategories();
?>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script>
    // add missing values to the dropdowns
    var locations = <?php echo $locations; ?>;
    for (i = 0; i < locations.length; ++i) {
        var value = locations[i].toLowerCase().replace(/ /g, '');
        if (!$("select[name='location']").find("option[value='" + value + "']").length) {
            $("select[name='location']").append($('<option>', {
                value: value,
                text: locations[i]
            }));
        }
    }
    var categories = <?php echo $categories; ?>;
    for (i = 0; i < categories.length; ++i) {
        var value = categories[i].toLowerCase().replace(/ /g, '');
        if (!$("select[name='category']").find("option[value='" + value + "']").length) {
            $("select[name='category']").append($('<option>', {
                value: value,
                text: categories[i]
            }));
        }
    }
</script>
<div class="container">
    <h2>Results</h2>
    <p><?php if ($event->searchTerm) { echo $event->searchTerm; } ?></p>            
    <table class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Start</th>
                <th>Finish</th>
                <th>Category</th>
                <th>Location</th>
            </tr>
        </thead>
        <tbody>
<?php
if (count($results)) {
    foreach ($results as $result) {
        echo '<tr>';
        echo '<td>' . $result['name'] . '</td>';
        echo '<td>' . $result['description'] . '</td>';
        echo '<td>' . $result['start'] . '</td>';
        echo '<td>' . $result['finish'] . '</td>';
        echo '<td>' . $result['category'] . '</td>';
        echo '<td>' . $result['location'] . '</td>';
        echo '</tr>';
    }
}
?>
        </tbody>
    </table>
</div>
