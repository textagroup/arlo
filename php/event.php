<?php

// autoload libraries installed with Composer
require 'vendor/autoload.php';

// load all the variables from the .env file
$dotenv = new Dotenv\Dotenv($_SERVER['DOCUMENT_ROOT']);
$dotenv->load();

/**
 * event class to query a database
 */
class event {

    private $dbServer;
    private $dbName;
    private $dbUsername;
    private $dbPassword;
    private $db;

    private $locations;

    public $searchTerm;

    function __construct() {
        // assign db details to variables
        $this->dbServer = getenv('DATABASE_SERVER');
        $this->dbName = getenv('DATABASE_NAME');
        $this->dbUsername = getenv('DATABASE_USERNAME');
        $this->dbPassword = getenv('DATABASE_PASSWORD');
        $dsn = "mysql:dbname={$this->dbName};host={$this->dbServer}";
        try {
            $this->db = new PDO($dsn, $this->dbUsername, $this->dbPassword);
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
    }

    /**
     * Main search class
     *
     * @param $keyword keyword to search db
     * @param $location location to search
     * @param $category category to search
     * @return array
     */
    public function search($keyword = null, $location = null, $category = null) {
        // call getLocations now so we can setup all the locations that are missing from unchanged.php
        $this->getLocations();
        $results = array();
        $params = array();
            $sql =
                "SELECT * FROM custom_events WHERE finish > NOW()";
        if ($keyword) {
            $sql .=
                " AND (description LIKE ? OR name LIKE ?)";
            $params[] = "%$keyword%";
            $params[] = "%$keyword%";
            $this->searchTerm = 'Searched on the keyword <a href = "?keyword=' . strip_tags($keyword) . '">'
                . strip_tags($keyword) . '</a>';
        }
        if ($location) {
            $sql .= " AND location = ?";
            $params[] = $this->mapLocations($location);
            $this->searchTerm = ($this->searchTerm)
                ? $this->searchTerm . ' and the location <a href = "?location=' . strip_tags($location) . '">'
                    . strip_tags($this->mapLocations($location)) . '</a>'
                : 'Searched on the location <a href = "?location=' . strip_tags($location) . '">' 
                    . strip_tags($this->mapLocations($location)) . '</a>';
        }
        if ($category) {
            $sql .= " AND category = ?";
            $params[] = ucfirst($category);
            $this->searchTerm = ($this->searchTerm)
                ? $this->searchTerm . ' and the category <a href = "?category=' . strip_tags($category) . '">'
                    . strip_tags($category) . '</a>'
                : 'Searched on the category <a href = "?category=' . strip_tags($category) . '">'
                    . strip_tags($category) . '</a>';
        }
        $query = $this->db->prepare($sql);
        if ($query->execute($params)) {
            while ($row = $query->fetch()) {
                $results[] = $row;
            }
        }

        return $results;
    }

    /**
     * query the location for a display value
     *
     * @param $location location to query
     * @return string
     */
    public function mapLocations($location) {
        if (isset($this->locations)) {
            return $this->locations[$location];
        }
        return $location;
    }

    /**
     * query the database for all available locations
     * Used by jQuery to populate the missing values
     *
     * @param string json
     */
    public function getLocations() {
        $locations = array();
        $sql = 'SELECT DISTINCT(location) FROM custom_events';
        $query = $this->db->query($sql);
        while ($row = $query->fetch()) {
            $locations[] = $row['location'];
        }

        // add locations to the locations property
        foreach ($locations as $location) {
            $value = preg_replace('/\s+/', '', strtolower($location));
            $this->locations[$value] = $location;
        }
        return json_encode($locations);
    }

    /**
     * query the database for all available categories
     * Used by jQuery to populate the missing values
     *
     * @param string json
     */
    public function getCategories() {
        $categories = array();
        $sql = 'SELECT DISTINCT(category) FROM custom_events';
        $query = $this->db->query($sql);
        while ($row = $query->fetch()) {
            $categories[] = $row['category'];
        }
        return json_encode($categories);
    }
}
